﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Pluralsight.Startup))]
namespace Pluralsight
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
