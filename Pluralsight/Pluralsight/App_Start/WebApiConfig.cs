﻿using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Web;
using System.Web.Http;

namespace Pluralsight.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration configuration)
        {
            configuration.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));

            var jsonFormatter = configuration.Formatters.OfType<JsonMediaTypeFormatter>().First();
            //use camel case when resolve the contract
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            configuration.Routes.MapHttpRoute("RepliesRoute",
                 "api/v1/topics/{topicid}/replies/{id}",
                new { controller = "replies", id = RouteParameter.Optional });

            configuration.Routes.MapHttpRoute("API Default", 
                 "api/v1/topics/{id}",
                new { controller = "topics", id = RouteParameter.Optional });
        }
    }
}