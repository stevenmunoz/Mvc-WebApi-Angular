﻿
var module = angular.module("homeIndex", ['ngRoute']);

module.config(['$routeProvider',
  function ($routeProvider) {
      $routeProvider.
        when('/', {
            templateUrl: '/templates/topicsView.html',
            controller: 'topicsController'
        }).
        when('/newmessage', {
            templateUrl: '/templates/newTopicView.html',
            controller: 'newTopicController'
        }).
        when('/newmessage/:id', {
            templateUrl: '/templates/singleTopicView.html',
            controller: 'singleTopicController'
        }).
        otherwise({
            redirectTo: '/'
        });
  }]);

module.factory('dataService', ["$http", "$q",

    function ($http, $q) {
   
    var _topics = [];
    var _isInit = false;

    var _isReady = function () {
        return _isInit;
    }

    var _getTopics = function () {

        var deferred = $q.defer();

        $http.get("/api/v1/topics?includeReplies=true").then(function (result) {
            //succesful
            //$scope.data = result.data;
            //it's suggested for collections use specific angular js function copy
            //which copy from origin to destiny, it allows to angular know needs to iterate, and
            //reload the directives
            angular.copy(result.data, _topics);
            _isInit = true;
            deferred.resolve();
        },
       function () {
           //error
           deferred.reject();
       });

        return deferred.promise;
    };

    var _addTopic = function (newTopic) {
        var deferred = $q.defer();

        $http.post("/api/v1/topics", newTopic)
           .then(function (result) {
               var newlyCreatedTopic = result.data;
               _topics.splice(0, 0, newlyCreatedTopic);
               //in case user needs the object
               deferred.resolve(newlyCreatedTopic);
           },
           function () {
               deferred.reject();
           }
       );

        return deferred.promise;
    };

    //truly private implementation of function
    function _findTopic(id)
    {
        var found = null;

        $.each(_topics, function (i, item) {

            if (item.id == id)
            {
                found = item;
                //false allow us stop when we find desired object
                //instead angular js iterate mechanism force us to 
                //iterate all collection
                return false;
            }
        });

        return found;
    }

    var _getTopicsById = function (id) {

        var deferred = $q.defer();

        if (_isReady()) {

            var topic = _findTopic(id);
            console.log(topic);
            if (topic) {
                deferred.resolve(topic);
            } else
            {
                deferred.reject();
            }
        } else
        {
            _getTopics().then(function () {
                //success
                var topic = _findTopic(id);
                if (topic) {
                    deferred.resolve(topic);
                } else {
                    deferred.reject();
                }
            },
            function () {
                //error
                deferred.reject();
            });
        }

        return deferred.promise;
    }

    var _saveReply = function (topic, newReply) {
        var deferred = $q.defer();

        $http.post("api/v1/topics/" + topic.id + "/replies", newReply)
            .then(function (result) {
                if (topic.replies == null) topic.replies = [];
                topic.replies.push(result.data);
                deferred.resolve(result.data);
            },
            function () {
                deferred.reject();
            })
            

        return deferred.promise;
    }

    //singleton object
    return {
        topics: _topics,
        getTopics: _getTopics,
        addTopic: _addTopic,
        isReady: _isReady,
        getTopicsById: _getTopicsById,
        saveReply: _saveReply
    };
}]);

module.controller("topicsController", ["$scope", "$http", "dataService",

    function ($scope, $http, dataService) {
    $scope.data = dataService;
    $scope.isBussy = false;

    if (dataService.isReady() == false) {

        $scope.isBussy = true;
        dataService.getTopics().then(function () {
            //success
        },
        function () {
            //error
            alert("Could not load topics");
        })
        .then(function () {
            $scope.isBussy = false;
        });
    }
}]);

module.controller("newTopicController", ["$scope", "$http", "$window", "dataService",

    function ($scope, $http, $window, dataService) {
    $scope.newTopic = {};

    $scope.save = function () {
       
        dataService.addTopic($scope.newTopic).then(function () {
            //success
            $window.location = "/#"
        },
        function () {
            //error
            alert("Could not save the new topic");
        });
    };

}]);

module.controller("singleTopicController", ["$scope", "dataService", "$window", "$routeParams",

    function ($scope, dataService, $window, $routeParams) {

    $scope.topic = null;
    $scope.newReply = {};

    dataService.getTopicsById($routeParams.id)
        .then(function (topic) {
            $scope.topic = topic;
        },
        //error case, invalid id, bad uri
        function () {
            $window.location = "#/";
        });

    $scope.addReply = function () {
        dataService.saveReply($scope.topic, $scope.newReply)
          .then(function () {
              $scope.newReply.body = "";
          },
          function () {
              alert("Could not save the new reply");
          });
    };
}]);