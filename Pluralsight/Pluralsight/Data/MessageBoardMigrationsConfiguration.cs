﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;

namespace Pluralsight.Data
{
    public class MessageBoardMigrationsConfiguration : DbMigrationsConfiguration<MessageBoardContext>
    {
        public MessageBoardMigrationsConfiguration()
        {
            this.AutomaticMigrationDataLossAllowed = true;
            this.AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(MessageBoardContext context)
        {
            base.Seed(context);

#if DEBUG
            if (context.Topics.Count() == 0) 
            {
                var topic = new Topic()
                {
                    Title = "I Love Mvc",
                    Created = DateTime.Now,
                    Body = "I Love Asp.net Mvc and I want everyone to know",
                    Replies = new List<Reply>()
                    {
                        new Reply () 
                        {
                            Body = "I love it too !",
                            Created = DateTime.Now
                        },
                        new Reply () 
                        {
                            Body = "Me too!",
                            Created = DateTime.Now
                        },
                        new Reply () 
                        {
                            Body = "Nahh it sucks °.° !",
                            Created = DateTime.Now
                        }
                    }
                };

                context.Topics.Add(topic);

                try 
                {
                    context.SaveChanges();
                }
                catch (Exception ex) 
                {
                    var msg = ex.Message;
                }
            }                
#endif
        }
    }
}
