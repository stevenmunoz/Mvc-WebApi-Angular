﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Pluralsight.Models
{
    public class ContactModel
    {
        public string name { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string comment { get; set; }
    }
}
